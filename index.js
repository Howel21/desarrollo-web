var express = require('express'); 
var app=express();

app.get('/', function(req, res) {
res.send('Hola, Bienvenido a Express');
});

app.use(express.static('public'));

app.listen(4300, function(){
    console.log('Aplicación ejemplo, escuchando el puerto 4300');
});