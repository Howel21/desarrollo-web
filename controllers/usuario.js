var Usuario = require('../models/usuario');

module.exports.list = function(req, res) {
    Usuario.getAllUsuarios().then(function(usuarios) {
        res.render('usuarios/index', { usuarios: usuarios });
    });
};

module.exports.create_get = function(req, res) {
    res.render('usuarios/create', { usuario: {}, errors: {} });
};

module.exports.create_post = function(req, res) {
    var usuario = { nombre: req.body.nombre, email: req.body.email, password: req.body.password };

    if (req.body.password !== req.body.confirm_password) {
        res.render('usuarios/create', { usuario: usuario, errors: { confirm_password: { message: "El password de confirmacion no es igual al password ingresado" }}});
        return;
    }

    // Add new usuario
    Usuario.add(usuario).then(function(nuevoUsuario) {
        // Send confirmation email
        nuevoUsuario.sendConfirmationEmail().finally(function() {
            // Redirect to List page
            res.redirect('/usuarios');
        });
    }).catch(function(error) {
        console.log(error);
        // throw the error to let the most outside catch to handle it
        res.render('usuarios/create', { usuario: usuario, errors: error.errors });
    });
};

module.exports.update_get = function(req, res) {
    Usuario.findById(req.params.id).then(function(usuario) {
        res.render('usuarios/update', { usuario: usuario, errors: {} });
    });
};

module.exports.update_post = function(req, res) {
    var data = {
        nombre: req.body.nombre
    };
    
    Usuario.update(req.params.id, data).then(function() {
        // Redirect to List page
        res.redirect('/usuarios');
    }).catch(function(error) {
        var usuario = {
            id: req.params.id,
            nombre: req.body.nombre
        }
        console.log(error);
        // Stay in update's page and show error
        res.render('usuarios/update', { usuario: usuario, errors: error.errors });
    });
};

module.exports.delete_post = function(req, res) {
    // Delete usuario
    Usuario.deleteById(req.params.id).then(function() {
        // Redirect to List page
        res.redirect('/usuarios');
    });
};
