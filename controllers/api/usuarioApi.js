var Usuario = require('../../models/usuario');

module.exports.list = function(req, res) {
    Usuario.getAllUsuarios().then(function(usuarios) {
        res.status(200).json({usuarios});
    });
};

module.exports.reserve = function(req, res) {
    Usuario.findById(req.params.id).then(function(usuario) {
        // Reservar
        usuario.reserve(req.body.biciId, req.body.desde, req.body.hasta).then(function(reserva) {
            res.status(201).json(reserva);
        }).catch(function(error) {
            // throw the error to let the most outside catch to handle it
            throw Error(error);
        });
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error reserving'
        });
    });
};

module.exports.create = function(req, res) {
    var usuario = Usuario.createInstance(req.body.nombre);

    // Add new Usuario
    Usuario.add(usuario).then(function(newUser) {
        res.status(201).json(newUser);
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error adding Usuario'
        });
    });
};

module.exports.update = function(req, res) {
    var data = {
        nombre: req.body.nombre
    };
    
    // Update bici
    Usuario.update(req.params.id, data).then(function(response) {
        Usuario.findById(req.params.id).then(function(usuario) {
            res.status(200).json(usuario);
        }).catch(function(error) {
            // throw the error to let the most outside catch to handle it
            throw Error(error);
        });
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error updating Usuario'
        });
    });
};

module.exports.delete = function(req, res) {
    // Delete usuario
    Usuario.deleteById(req.params.id).then(function() {
        res.status(204).send();
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error deleting Usuario'
        });
    });
};
