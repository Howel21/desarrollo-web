const Bicicleta = require("../../models/bicicleta");


//Funcion para listar bicicletas por medio de un endpoint
exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis((err, bicicletas) => {
        res.status(200).json({
            bicicletas
        })
    })
}

//Funcion para crear una bicicleta por medio de un endpoint
exports.bicicleta_create = function (req, res) {
    const { color, modelo, lat, lng } = req.body;
    const bici = new Bicicleta({ color, modelo, ubicacion: [lat, lng] });
    Bicicleta.add(bici);

    res.status(201).json({
        bicicleta: bici
    });
}

//Funcion para eliminar una bicicleta por medio de un endpoint, enviando un ID por el body
exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.id, (err, success) => {
        if (err) console.log(err)
        res.status(204).send();
    });
}

//Funcion para editar una bicicleta por medio de un endpoint, enviando el ID de la bicicleta a editar
//por medio del parametro ID
exports.bicicleta_update = function (req, res) {
    const { code, color, modelo, lat, lng } = req.body;

    Bicicleta.updateOne({ code: req.params.id }, req.body, (err, success) => {
        if (err) console.log(err);
        res.status(200).json({ message: "Update exitoso" })
    });
}