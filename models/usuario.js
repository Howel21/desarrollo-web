const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Reserva = require('./reserva');
const crypto = require('crypto');
const Token = require('../models/token');
const bcrypt = require('bcrypt');
const mailer = require('../mailer/mailer');
const Schema = mongoose.Schema;

const saltRounds = 10;

const validateEmail = function (email) {
    const reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return reg.test(email);
};

const usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio'],
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [
            /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        ],
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio'],
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false,
    },
    googleId: String,
    facebookId: String,
});

usuarioSchema.plugin(uniqueValidator, {
    message: 'El {PATH} ya existe con otro usuario.',
});

usuarioSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(
    condition,
    callback
) {
    const self = this;
    console.log(condition);
    self.findOne(
        {
            $or: [
                { googleId: condition.id },
                { email: condition.emails[0].value },
            ],
        },
        function (err, result) {
            if (result) {
                callback(err, result);
            } else {
                let values = {};
                values.googleId = condition.id;
                values.email = condition.email[0].values;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition._json.etag;
                self.create(values, (err, result) => {
                    if (err) console.log(err);
                    return callback(err, result);
                });
            }
        }
    );
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(
    condition,
    callback
) {
    const self = this;
    console.log(condition);
    self.findOne(
        {
            $or: [
                { facebookId: condition.id },
                { email: condition.emails[0].value },
            ],
        },
        function (err, result) {
            if (result) {
                callback(err, result);
            } else {
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.email[0].values;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                self.create(values, (err, result) => {
                    if (err) console.log(err);
                    return callback(err, result);
                });
            }
        }
    );
};

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    const reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde,
        hasta,
    });
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.resetPassword = function (cb) {
    const token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString('hex'),
    });
    const email_destination = this.email;
    token.save(function (err) {
        if (err) {
            return cb(err);
        }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password de cuenta',
            text:
                'Hola, \n\n' +
                'Por favor, para resetear el password de su cuenta haga click en este link: \n' +
                'http://localhost:5000' +
                '/resetPassword/' +
                token.token +
                '.\n',
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return cb(err);
            }
            console.log(
                'Se envio un email para resetear el password a: ' +
                    email.email_destination +
                    '.'
            );
        });

        cb(null);
    });
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString('hex'),
    });
    const email_destination = this.email;
    token.save(function (err) {
        if (err) {
            return console.log(err.message);
        }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text:
                'Hola, \n\n' +
                'Por favor, para verificar su cuenta haga click en este link: \n' +
                'http://localhost:5000' +
                '/token/confirmation/' +
                token.token +
                '.\n',
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return cb(err);
            }
            console.log(
                'Se envio un email de verificacion a: ' +
                    email.email_destination +
                    '.'
            );
        });
    });
};

module.exports = mongoose.model('Usuario', usuarioSchema);
